#import <Foundation/Foundation.h>
#import "Teacher.h"
#import "Student.h"
#import "Person.h"

@implementation Teacher
// init method 
- (id) init:(NSString *)name birthday:(int)d month:(int)m year:(int)y {
    self = [super init:name birthday:d month:m year:y];
    return (self);
}
    
- (int) checkBirthDay:(int)d andMonth:(int)m andYear:(int)y {
    if (d<1 || d>31 || m <1 || m>12) {
        return 0;
    }
    else {
        if (m==4||m==6||m==9||m==11) {
            if (d==31) {
                return 0;
            } else {
                return 1;
            }
        } else {
            if (m==2) {
                if (d > 29) {
                    return 0;
                } else {
                    if (d==29) {
                        if (IS_LEAP_YEAR(y)) {
                            return 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 1;
                    }
                }
            } else {
                return 1;
            }
        }
    }
}
- (NSString*)checkName:(char *)name {
    NSString *str_name = [[NSString alloc] init];
    str_name = [NSString stringWithCString:name encoding:1];
    NSCharacterSet * setName = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ"] invertedSet];
    if([str_name rangeOfCharacterFromSet:setName].location != NSNotFound) {
        return NULL;
    }
    else{
        return str_name;
    }
}

+ (float) isFloatValue:(char *)score {
    NSString *str_score = [[NSString alloc] init];
    str_score = [NSString stringWithCString:score encoding:1];
    NSCharacterSet * setScore = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890."] invertedSet];
    if([str_score rangeOfCharacterFromSet:setScore].location != NSNotFound) {
        return 0;
    }
    else{
        float rs = [str_score floatValue];
        return rs;
    }

}
- (NSMutableString*) checkClass:(char *)c {
    NSMutableString *str_class = [[NSMutableString alloc] init];
    str_class = [NSMutableString stringWithCString:c encoding:1];
    NSCharacterSet * setClass = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890"] invertedSet];
    if([str_class rangeOfCharacterFromSet:setClass].location != NSNotFound) {
        return NULL;
    }
    else{
        return str_class;
    }

}

- (int)takeInfoStudent:(Student *)std{
    char tmp_name[256];
    printf("Full Name: ");
    scanf("%s", tmp_name);
    NSString *name = [self checkName:tmp_name];
    if(name == NULL) {
        printf("Invalid name\n");
        return 0;
    }
    else{
        [std setFullName:name];
    }
   
    char tmp_bd[10];
    printf("Birthday(dd/mm/yyyy): ");
    scanf("%s", tmp_bd);
    NSString *birthday = [[NSString alloc] init];
    birthday = [NSString stringWithCString:tmp_bd encoding:1];
    NSArray *arr = [[NSArray alloc] init];
    arr = [birthday componentsSeparatedByString:@"/"];
    if([arr count] != 3) {
        printf("Invalid birthday\n");
        return 0;
    }
    else{
        int d=(int)[arr[0] integerValue];
        int m = (int)[arr[1] integerValue];
        int y = (int)[arr[2] integerValue];
        if([self checkBirthDay:d andMonth:m andYear:y] == 1) {
            [std setBirthday:d :m :y];
        }
        else {
            printf("Invalid birthday\n");
            return 0;
        }
    }
    
    char tmp_class[5];
    printf("Class: ");
    scanf("%s", tmp_class);
    NSMutableString *class = [[NSMutableString alloc] init];
    class = [self checkClass:tmp_class];
    
    if(class == NULL) {
        printf("Invalid class\n");
        return 0;
    }
    else{
        [std setClass:class];
    }
    
    float scoreMath, scorePhysic, scoreChemistry, scoreEnglish;
    char tmp_sm[5];
    printf("Math Score:");
    scanf("%s", tmp_sm );
    scoreMath = [Teacher isFloatValue:tmp_sm];
    if(scoreMath == 0) {
        printf("Invalid math score\n");
        return 0;
    }
    else {
       [std setScoreMath:scoreMath];
    }
        
    char tmp_sp[5];
    printf("Physic Score:");
    scanf("%s", tmp_sp );
    scorePhysic = [Teacher isFloatValue:tmp_sp];
    if(scorePhysic == 0) {
        printf("Invalid physic score\n");
        return 0;
    }
    else {
        [std setScorePhysic:scorePhysic];
    }
    
    char tmp_sc[5];
    printf("Chemistry Score:");
    scanf("%s", tmp_sc );
    scoreChemistry = [Teacher isFloatValue:tmp_sc];
    if(scoreChemistry == 0) {
        printf("Invalid chemistry score\n");
        return 0;
    }
    else {
        [std setScoreChemistry:scoreChemistry];
    }
        
    char tmp_se[5];
    printf("English Score:");
    scanf("%s", tmp_se );
    scoreEnglish = [Teacher isFloatValue:tmp_se];
    if(scoreEnglish == 0) {
        printf("Invalid english score\n");
        return 0;
    }
    else {
        [std setScoreEnglish:scoreEnglish];
    }
    return 1;
}
- (Student*) searchByName:(NSMutableArray *)arr findStudent:(NSString *)name {
    for(int i = 0; i < [arr count]; i++)
    {
        if([[arr[i] getFullName] isEqualToString:name])
            return arr[i];
    }
    return NULL;
}

- (Student*)searchByScoreMath:(NSMutableArray*)arr mathScore:(float)ms {
    for(int i = 0; i < [arr count]; i++) {
        if([arr[i] getScoreMath] == ms)
            return arr[i];
    }
    return NULL;
}

- (Student*)searchByScorePhysic:(NSMutableArray *)arr physicScore:(float)ps {
    for(int i = 0; i < [arr count]; i++) {
        if([arr[i] getScorePhysic] == ps)
            return arr[i];
    }
    return NULL;
}

- (Student*)searchByScoreChemistry:(NSMutableArray *)arr chemistryScore:(float)cs {
    for(int i = 0; i < [arr count]; i++) {
        if([arr[i] getScoreChemistry] == cs)
            return arr[i];
    }
    return NULL;
}

- (Student*)searchByScoreEnglish:(NSMutableArray *)arr englishScore:(float)es {
    for(int i = 0; i < [arr count]; i++) {
        if([arr[i] getScoreEnglish] == es)
            return arr[i];
    }
    return NULL;
}
- (Student*)searchByScoreAverage:(NSMutableArray *)arr avgScore:(float)as{
    for(int i = 0; i < [arr count]; i++) {
        if([arr[i] getAverageScore] == as)
            return arr[i];
    }
    return NULL;
}

-(NSMutableArray*)sortByAverageScore:(NSMutableArray*)arr{
    for(int i=0; i<[arr count]-1; i++) {
        for(int j=1; j<[arr count]; j++) {
            if ([arr[i] getAverageScore] > [arr[j] getAverageScore]) {
                Student* tmp = [[Student alloc] init];
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
        }
    }
    return arr;
}
- (int) removeStudent:(NSMutableArray*)arr rmStudent:(char *)name {
    NSString * str_name = [self checkName:name];
    if (str_name == NULL) {
        return 1;
    } else {
        Student* std = [self searchByName:arr findStudent:str_name];
        NSInteger index = [arr indexOfObject:std];
        if(index == NSNotFound) {
            return 2;
        }
        else {
            [arr removeObjectAtIndex:index];
            return 0;
        }
    }
}
- (int) insertStudent:(NSMutableArray*)arr insertStudent:(Student *)std atIndex:(NSInteger)index {
    if( index > [arr count]){
        [arr insertObject:std atIndex:[arr count]];
        return 1;
    }
    else {
        [arr insertObject:std atIndex:index];
        return 0;
    }
}
@end


