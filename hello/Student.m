//
//  Student.m
//  hello
//
//  Created by Mac365.vn on 3/14/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Student.h"

@implementation Student

- (id)init:(NSString *)name ofClass:(NSMutableString*)c birthday:(int)d month:(int)m year:(int)y Mathscore:(float)sm scorePhysic:(float)sp scoreChemistry:(float)sc scoreEnglish:(float)se {
    self = [super init:name birthday:d month:m year:y];
    class = c;
    scoreMath = sm;
    scorePhysic = sp;
    scoreChemistry = sc;
    scoreEnglish = se;
    return (self);
}

- (NSMutableString*) getClass {
    return class;
}
- (float) getScoreMath {
    return scoreMath;
}
- (float) getScorePhysic {
    return scorePhysic;
}
- (float) getScoreChemistry {
    return scoreChemistry;
}
- (float) getScoreEnglish {
    return scoreEnglish;
}
- (float) getAverageScore{
    float average_score = (scoreMath + scorePhysic + scoreChemistry + scoreEnglish)/4;
    return average_score;
}
- (void) showInfo {
    NSLog(@"\n \
          Full Name: %@\n \
          Birthday: %d/%d/%d\n \
          Class: %@\n \
          Math Score: %.2f\n \
          Physic Score: %.2f\n \
          Chemistry Score: %.2f\n \
          English Score: %.2f\n \
          Average Score: %.2f\n \
          ", fullName,birthDay.day, birthDay.month, birthDay.year, class, scoreMath, scorePhysic, scoreChemistry, scoreEnglish, [self getAverageScore]);
}

- (void) setClass:(NSMutableString *)c {
    class = c;
}
- (void) setScoreMath:(float)sm {
    scoreMath = sm;
}
- (void) setScorePhysic:(float)sp {
    scorePhysic = sp;
}
- (void) setScoreChemistry:(float)sc {
    scoreChemistry = sc;
}
- (void) setScoreEnglish:(float)se {
    scoreEnglish = se;
}
@end
