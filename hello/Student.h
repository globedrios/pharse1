//
//  Student.h
//  hello
//
//  Created by Mac365.vn on 3/14/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
//
#import "Person.h"

@class Student;
@interface Student : Person {
    NSMutableString *class;
    float scoreMath;
    float scorePhysic;
    float scoreChemistry;
    float scoreEnglish;
}
// init method
- (id) init:(NSString*)name ofClass:(NSMutableString*)c birthday:(int)d month:(int)m year:(int)y Mathscore:(float)sm scorePhysic:(float)sp scoreChemistry:(float)sc scoreEnglish:(float)se;

// get methods
- (NSMutableString*) getClass;
- (float) getScoreMath;
- (float) getScorePhysic;
- (float) getScoreChemistry;
- (float) getScoreEnglish;
- (float) getAverageScore;
- (void) showInfo;



// set methods
- (void) setClass:(NSMutableString*)c;
- (void) setScoreMath:(float)sm;
- (void) setScorePhysic:(float)sp;
- (void) setScoreChemistry:(float)sc;
- (void) setScoreEnglish:(float)se;
@end

