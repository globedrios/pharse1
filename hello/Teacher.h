//
//  Teacher.h
//  hello
//
//  Created by Mac365.vn on 3/14/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
#import "Student.h"
#import "Person.h"
#define IS_LEAP_YEAR(y) y % 4 == 0 && y % 100 != 0 || y % 400 == 0
//
@interface Teacher : Person {
}

// init method
-(id) init:(NSString *)name birthday:(int)d month:(int)m year:(int)y;

// check methods
- (int)checkBirthDay:(int)d andMonth:(int)m andYear:(int)y;
- (NSString*)checkName:(char*)name;
+ (float)isFloatValue:(char*)score;
- (NSMutableString*) checkClass:(char*)c;

// tasks
- (int)takeInfoStudent:(Student*)std;
- (Student*)searchByName:(NSMutableArray*)arr findStudent:(NSString*)name;
- (Student*)searchByScoreMath:(NSMutableArray*)arr mathScore:(float)ms; 
- (Student*)searchByScorePhysic:(NSMutableArray*)arr physicScore:(float)ps;
- (Student*)searchByScoreChemistry:(NSMutableArray*)arr chemistryScore:(float)cs;
- (Student*)searchByScoreEnglish:(NSMutableArray*)arr englishScore:(float)es;
- (Student*)searchByScoreAverage:(NSMutableArray*)arr avgScore:(float)as;
- (NSMutableArray*)sortByAverageScore:(NSMutableArray*)arr;
- (int) removeStudent:(NSMutableArray*)arr rmStudent:(char*)name ;
- (int) insertStudent:(NSMutableArray*)arr insertStudent:(Student*)std atIndex:(NSInteger)index;
@end
