//
//  test.m
//  hello
//
//  Created by Mac365.vn on 3/14/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Student.h"
#import "Teacher.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        Teacher *t = [[Teacher alloc] init:@"Tran" birthday:31 month:12 year:1890];
        NSMutableArray *listStudent = [[NSMutableArray alloc] init ];
        Student *std1 = [[Student alloc] init:@"NguyenVanA" ofClass:[@"12a" mutableCopy] birthday:1 month:1 year:1990 Mathscore:7.0 scorePhysic:8.0 scoreChemistry:9.0 scoreEnglish:10.0 ];
        [listStudent addObject:std1];
        Student *std2 = [[Student alloc] init:@"NguyenVanB" ofClass:[@"11a" mutableCopy] birthday:2 month:2 year:1991 Mathscore:6.7 scorePhysic:7.8 scoreChemistry:8.9 scoreEnglish:9.0];
        [listStudent addObject:std2];
        Student *std3 = [[Student alloc] init:@"NguyenVanC" ofClass:[@"10a" mutableCopy] birthday:3 month:3 year:1993 Mathscore:9.8 scorePhysic:8.7 scoreChemistry:7.6 scoreEnglish:6.5];
        [listStudent addObject:std3];
        Student *result = [[Student alloc] init];
        int c;
        int opt;
        do {
            NSLog(@"\n \
                   ======= STUDENT MANAGEMENT =======\n \
                   1. Show information of student\n \
                   2. Input infomation of student\n \
                   3. Sort student by average score\n \
                   4. Search student by name\n \
                   5. Search student by math score\n \
                   6. Search student by physic score\n \
                   7. Search student by chemistry score\n \
                   8. Search student by english score\n \
                   9. Search student by average score\n \
                   10. Remove student\n \
                   11. Insert student\n \
                   Choose number!\n ");
            scanf("%i", &c);
            switch (c) {
                case 1: {
                    NSLog(@"======= INFORMATION OF STUDENT =======");
                    for (int i = 0; i < [listStudent count]; i++) {
                        [listStudent[i] showInfo];
                    }
                    break;
                }
                case 2:{
                    NSLog(@"Please input information of student:");
                    int flag = 0;
                    do {
                        Student *std = [[Student alloc] init];
                        flag = [t takeInfoStudent:std];
                        if(flag == 1) {
                            [listStudent addObject:std];
                            [std showInfo];
                        }
                    }while(flag == 0);
                    break;
                }
                case 3: {
                    [t sortByAverageScore:listStudent];
                    for(int i=0; i<[listStudent count]; i++){
                        [listStudent[i] showInfo];
                    }
                    break;
                }
                case 4: {
                    NSLog(@"Find Name: ");
                    char tmp_name[100];
                    scanf("%s", tmp_name);
                    NSString *name = [[NSString alloc] init];
                    name = [NSString stringWithCString:tmp_name encoding:1];
                    result = [t searchByName:listStudent findStudent:name];
                    if (result == NULL) {
                        NSLog(@"Not found");
                    }
                    else {
                        [result showInfo];
                    }
                    break;
                }
                case 5: {
                    printf("Math score: ");
                    char f[5];
                    scanf("%s",f);
                    float ms = [Teacher isFloatValue:f];
                    if(ms == 0) {
                        NSLog(@"Invalid number");
                    }
                    else {
                        result = [t searchByScoreMath:listStudent mathScore:ms];
                        if(result == NULL){
                            NSLog(@"Not found");}
                        else{
                            [result showInfo];}
                    }
                    break;
                }
                case 6: {
                    printf("Physic score: ");
                    char f[5];
                    scanf("%s",f);
                    float ps = [Teacher isFloatValue:f];
                    if(ps == 0) {
                        NSLog(@"Invalid number");
                    }
                    else {
                        result = [t searchByScorePhysic:listStudent physicScore:ps];
                        if(result == NULL){
                            NSLog(@"Not found");}
                        else{
                            [result showInfo];}
                    }
                    break;
                }
                case 7: {
                    printf("Chemistry score: ");
                    char f[5];
                    scanf("%s",f);
                    float cs = [Teacher isFloatValue:f];
                    if(cs == 0) {
                        NSLog(@"Invalid number");
                    }
                    else {
                        result = [t searchByScoreChemistry:listStudent chemistryScore:cs];
                        if(result == NULL){
                            NSLog(@"Not found");}
                        else{
                            [result showInfo];}
                    }
                    break;
                }
                case 8: {
                    printf("English score: ");
                    char f[5];
                    scanf("%s",f);
                    float es = [Teacher isFloatValue:f];
                    if(es == 0) {
                        NSLog(@"Invalid number");
                    }
                    else {
                        result = [t searchByScoreEnglish:listStudent englishScore:es];
                        if(result == NULL){
                            NSLog(@"Not found");}
                        else{
                            [result showInfo];}
                    }
                    break;
                }
                case 9: {
                    printf("Average score: ");
                    char f[5];
                    scanf("%s",f);
                    float as = [Teacher isFloatValue:f];
                    if(as == 0) {
                        NSLog(@"Invalid number");
                    }
                    else {
                        result = [t searchByScoreAverage:listStudent avgScore:as];
                        if(result == NULL){
                            NSLog(@"Not found");}
                        else{
                            [result showInfo];}
                    }
                    break;
                }
                case 10: {
                    char name[256];
                    printf("Input name you want to remove: ");
                    scanf("%s", name);
                    int rs = [t removeStudent:listStudent rmStudent:name];
                    if (rs == 0) {
                        NSLog(@"Done!");
                    }
                    if (rs == 1) {
                        NSLog(@"Invalid name");
                    }
                    if (rs == 2) {
                        NSLog(@"Not found student to remove");
                    }
                    break;
                }
                case 11: {
                    NSLog(@"Please input information of student:");
                    int flag = 0;
                    do {
                        Student *std = [[Student alloc] init];
                        flag = [t takeInfoStudent:std];
                        if(flag == 1) {
                            int index, flg;
                            do {
                                printf("Please input index you want to insert: ");
                                scanf("%i", &index);
                                if(index < 0) {
                                    flg = 0;
                                }
                                else {
                                    flg = 1;
                                }
                            }while(flg==0);
                            int rs = [t insertStudent:listStudent insertStudent:std atIndex:(NSInteger)index];
                            if(rs == 1) {
                                NSLog(@"The index is large than length of list so student inserted at the end.");
                            }
                            else {
                                NSLog(@"Done.");
                            }
                        }
                    }while(flag == 0);
                    break;
                }                    
                default:
                    NSLog(@"Wrong number. Please choose again!");
                    break;
            }
            NSLog(@"Continue? (1(YES) or 0(NO))");
            scanf("%i", &opt);
        } while (opt == 1);
        return 0;
    }
}
