//
//  test.m
//  hello
//
//  Created by Mac365.vn on 3/14/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Student.h"
#import "Teacher.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Teacher *t = [[Teacher alloc] init];
        Student *std1 = [[Student alloc] init];
        Student *std2 = [[Student alloc] init];
        Student *std3 = [[Student alloc] init];
        Student *result = [[Student alloc] init];
        int c;
        char opt;
        do {
            NSLog(@"==menu==");
            
            scanf("%i", &c);
            switch (c) {
                case 1:{
                    NSLog(@"Please input 3 information of student:");
                    [t takeInfoStudent:std1];
                    //[t showInfo:(std1)];
                    [t takeInfoStudent:std2];
                    //[t showInfo:(std2)];
                    [t takeInfoStudent:std3];
                    //[t showInfo:(std3)];
                    break;}
                case 2: {
                    NSArray *sortedStudent = [[NSArray alloc] init];
                    sortedStudent = [t sortByAverageScore:std1 student2:std2 student3:std3];
                    for(int i = 0; i < [sortedStudent count]; i++)
                    {
                        result = [t searchByScoreAverage:[sortedStudent[i] floatValue] student1:std1 student2:std2 student3:std3];
                        if([result getFullName] == NULL) {
                            NSLog(@"No student");
                            break;}
                        else{
                            [t showInfo:result];}
                    }
                    break;}
                    
                default:
                    break;
            }
            NSLog(@"Continue? (y/n)");
            scanf("%c", &opt);
        } while (opt =='y');
        return 0;
    }
}
