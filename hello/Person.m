//
//  Person.m
//  hello
//
//  Created by Mac365.vn on 3/13/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@implementation Person
- (id) init:(NSString *)name birthday:(int)d month:(int)m year:(int)y {
    fullName = name;
    birthDay.day = d;
    birthDay.month = m;
    birthDay.year = y;
    return(self);
}

- (NSString*) getFullName {
    return fullName;
}

- (struct Date) getBirthday {
    return birthDay;
}

- (void) setFullName:(NSString *)name {
    fullName = name;
}

- (void) setBirthday:(int)d :(int)m :(int)y {
    birthDay.day = d;
    birthDay.month = m;
    birthDay.year = y;
}
@end
