//
//  Person.h
//  hello
//
//  Created by Mac365.vn on 3/13/17.
//  Copyright © 2017 Mac365.vn. All rights reserved.
//

struct Date {
    int day;
    int month;
    int year;
};

// construct class Person
@interface Person : NSObject {
    NSString *fullName;
    struct Date birthDay;
}
// init method
- (id) init:(NSString*)name birthday:(int)d month:(int)m year:(int)y;

// get methods
- (NSString*) getFullName;
- (struct Date) getBirthday;

// set methods
- (void)setFullName:(NSString*)name;
- (void)setBirthday:(int)d :(int)m :(int)y;

@end
